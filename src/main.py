#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtCore, QtGui
from add.gui import MainWindow
import sys

if __name__=="__main__":
    app = QtGui.QApplication(sys.argv)
    qb = MainWindow()
    qb.show()
    sys.exit(app.exec_())