#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL
from datetime import date, timedelta, time, datetime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import and_, or_
from util import Target, Grafik, FIO, DolgTarget, Dolg
from freeze import FreezeTable
import calendar
from pprint import pprint 

class TargetModel(object):
    'тип работы в графике'
    _data = {}
    def __init__(self, engine):
        Session = sessionmaker(bind=engine)
        self._session = Session()
        query = self._session.query(Target).all()
        for row in query:
            self._data[row.id] = row
    
    def getdata(self, targetid=0):
        if targetid == 0:
            return self._data
        else:        
            return self._data[targetid]

class DolgTargetModel(object):
    'список всех видов значений для графика по должности'
    _data = {} #словарь с ключами id и значениями типа target
    _datamenu = [] #список названий с ключами
    def __init__(self, engine):
        Session = sessionmaker(bind=engine)
        self._session = Session()
    
    def setdolg(self, dolgid):
        query = self._session.query(Target).filter(DolgTarget.dolgid == dolgid).all()
        self._data = {}
        for row in query:
            self._data[row.id] = row
    
    def getmenu(self):
        '''Возвращаем списко меню вида id, полное имя для меню'''
        data = []
        #сортировка словаря   
        for i,j in sorted(self._data.items(), key = lambda x: x[1].sort):
            data.append([j.id, j.namepopup])
        return data
    
    def gettarget(self, targetid):
        return self._data[targetid]

#//////////////////////////////////////////////////////////////////////////////////////
class FioModel(object):
    _data = [] #список фамилий
    def __init__(self, engine): 
        Session = sessionmaker(bind=engine)
        self._session = Session()
    
    def setdate(self, curdate):
        self._data = []
        #yearmonth - год, месяц, и любой день, по которому делаем график
        daybegin = date(curdate.year, curdate.month,1)
        dayend = date(curdate.year, curdate.month, calendar.monthrange(curdate.year,curdate.month)[1])
        query = self._session.query(FIO).filter(and_(FIO.datebegin <= dayend, or_(FIO.dateend == None, FIO.dateend>=daybegin))).all()
        for row in query:
            self._data += [row]
    
    def getdata(self):
        return self._data
#//////////////////////////////////////////////////////////////////////////////////////
class GrafikModel(object):
    'Ячейка на поле графика с различными расчетами'
    _grafik = None  #ссылка на элемент Grafik    
    _target = None  #ссылка на TargetModel
    def __init__(self, grafik, target):
        self._grafik = grafik
        self._target = target
        self.worktime()

    def worktime(self):
        'расчет рабочего времени'
        t = self._model.getdata(self._grafik.id)
        if t:
            print t.daybegin, t.dayend 

    def nighttime(self):
        return 0
#//////////////////////////////////////////////////////////////////////////////////////
class GroupDataModel(object):
    'группируем элементы, чаще всего по строке'
    _row        = 0             #по какой строке группировка
    _fioid      = 0             #id в таблице фио
    _fio        = ''            #фамилия
    _worktime   = timedelta(0,0)#время работы
    _nighttime  = timedelta(0,0)#ночное время работы
    _holiday    = timedelta(0,0)#праздничные
    _vedomost   = timedelta(0,0)#время по ведомости
    _data       = []            #список объектов GrafikModel
    _notgul     = 0             #количество отгулов
    _target     = None          #ссылка на TargetModel
    _dolgtarget = None          #ссылка на DolgTargetModel
    
    def __init__(self, engine, target):
        self._target = target
    
    def calc_data(self):
        #TODO временно добавляем значение
        self._data = [GrafikModel(self._target)]
        self._worktime   = timedelta(0,0)
        self._nighttime  = timedelta(0,0)
        self._holiday    = timedelta(0,0)
        self._vedomost   = timedelta(0,0)
        self._notgul     = 0
        
        for d in self._data:
            self._worktime += d.worktime()
            self._nighttime += d.nighttime()
#//////////////////////////////////////////////////////////////////////////////////////    
class TableModel(object):
    _observers = []
    _clear = [] #метка, что необходимо отрисовать
    _freezeColumnCount = 5   #количество фиксированных колонок
    _grdm = [] #список групповых элементов 
    _engine = None #движок sqlalchemy
    def __init__(self, engine):
        #Читаем фамилии, котрые програботали хоть день
        #получаем количество строк
        #по количеству строк получаем групповые элементы
        #получаем список всех элементов по таблице
        #в зависимости от строки добавляем в групповую
        self._target = TargetModel(engine) 
        self._dolgtarget = DolgTargetModel(engine)
        self._fio = FioModel(engine)
        self._clear = False
        self._engine = engine
        
    def setdate(self, curdate):
        self._clear = True
        self.notify()
        self._clear = False
        self._fio.setdate(curdate)
        self.notify()
    
    def getheader(self):
        'Печать заголовков, ФИО, часов и др. сумм'
        data = []        
        if not self._clear:
            self._grdm = []
            for fio in self._fio.getdata():
                self._grdm += [GroupDataModel(self._engine, self._target )]
                data += [[fio.fio,'0','0','0','0']]
        return data
    
    def gettable(self):
        'Печать табличных данных'
        data = []
        if not self._clear:
            data = [[1, 1, u'тест', 6], 
                [1, 2, u'тест', 2], 
                [1, 3, u'тест', 4],
                [1, 3, '']] #row,col,data
        return data
    
    def getmenu(self, row):#возвращаем список значений для меню
        dolgid = self._fio.getdata()[row].dolgid
        self._dolgtarget.setdolg(dolgid)
        menu = self._dolgtarget.getmenu()
        menu.append([0,''])
        menu.append([-1,u'Очистить'])
        #TODO хз как вызвать диалог дабы узнать как изменить время
        menu.append([-2,u'Изменить время'])
        return menu
    
    def setdata(self, range, value):
        'установить значения по полученному списку выбранных ячеек, и значения, которое необходимо установить'
        #range - где произвести изменения вида строка, колонка 
        #value - значение id какое выбрали в меню
        data = []
        for rowcol in range:
            if value == -1:
                v = ''
            else:
                v = str(value)
            
            data.append([rowcol.row(), rowcol.column() - self._freezeColumnCount , v, 0])
        return data  
    
    def append_observer(self, in_observer):
        self._observers.append(in_observer)
    def remove_observer(self, in_observer):
        self._observers.remove(in_observer)
    def notify(self):
        for i in self._observers:
            i.modelischanged()
#//////////////////////////////////////////////////////////////////////////////////////
class TableView(FreezeTable):
    _model = None
    _data = []
    def __init__(self, model, parent = None):
        FreezeTable.__init__(self, parent)
        self._model = model
        self._model.append_observer(self)
        self.freezeColumnCount = 5   #количество фиксированных колонок 
        self.rowCount = 3            #количество строк    
        self.headername = [u'Фамилия И.О.', u'Баланс',
                           u'Часов', 
                           u'Разница\nза год(час)', 
                           u'Отгулы\nколичество']#подписанные заголовки
        self.freeColumnCount = 30     #количество не фиксированных колонок
        self.redraw()
    
    def modelischanged(self):
        self._data = self._model.getheader()
        self.rowCount = len(self._data)
        if self.rowCount == 0:
            self.redraw()
        else:
            self._redraw()
            self.footredraw()
    
    def _redraw(self):
        #рисуем хедер
        for row, rowdata in enumerate(self._data):
            for col, cdata in enumerate(rowdata):
                self.itemtable(row, col, cdata)
        self._drawtable()
    
    def _drawtable(self, data = None):
        #рисуем таблицу
        if not data:
            data = self._model.gettable()
        print data
        for row in data:
            try:
                typedraw = row[3]
            except:
                typedraw = 0
            item = self.mdl.item(row[0], row[1] + self.freezeColumnCount)
            if item:
                item.setText(row[2])
            else:
                self.itemtable(row[0], row[1] + self.freezeColumnCount, row[2], typedraw)
                
    def contextMenuEvent(self, event):
        row = self.selectionModel().selectedIndexes()[0].row()
        menulist = self._model.getmenu(row)#получаем в виде id, название пункта меню
        menu = QtGui.QMenu()
        for i in menulist: 
            if i[0] == 0:
                menu.addSeparator()
            else:    
                act = QtGui.QAction(i[1], self)
                self.connect(act, SIGNAL("triggered()"), self.selectmenu)
                act.setData(i[0])
                menu.addAction(act)
        menu.exec_(event.globalPos())
                      
    def selectmenu(self):
        value = self.sender().data().toPyObject()
        rowcol = self.selectionModel().selectedIndexes()
        if value == -2: #Костыль для вызова диалога изменения времени по умолчанию
            dialog = DialogTime(self)
            dialog.setModal(True)
            now = datetime.now()
            time_ = time(now.hour, now.minute, now.second)
            dialog.settime(time_)
            dialog.setvalue(value)
            self.connect(dialog, SIGNAL("accepted()"), self.changetime)
            dialog.show()
            data = []
        else:
            data = self._model.setdata(rowcol, value)
        self._drawtable(data)
        self.footredraw()
        
    def changetime(self):
        'Получаем новое время'
        value = self.sender().getvalue()
        print self.sender().gettime(), value
        rowcol = self.selectionModel().selectedIndexes()
        data = self._model.setdata(rowcol, value)
        self._drawtable(data)
        self.footredraw() 
#//////////////////////////////////////////////////////////////////////////////////////    
class TableControl(object):
    def __init__(self, engine):
        self._model = TableModel(engine)
        self._view = TableView(self._model)
    
    def setdate(self, curdate):
        self._model.setdate(curdate)

    def getview(self):
        return self._view
#//////////////////////////////////////////////////////////////////////////////////////
class DialogTime(QtGui.QDialog):
    'Диалог для изменения времени по умолчанию'
    _value = 0#значение для хранения id 
    def __init__(self, parent =None):
        QtGui.QDialog.__init__(self, parent)
        gr = QtGui.QGridLayout()
        self._time = QtGui.QTimeEdit()
        self._time.setDisplayFormat('HH:mm')
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        
        gr.addWidget(self._time)
        gr.addWidget(btn1)
        self.setLayout(gr)
        self.setWindowTitle(u'Изменить время')
    
    def setvalue(self, value):
        self._value = value
    def getvalue(self):
        return self._value 
    
    def settime(self, time):
        self._time.setTime(QtCore.QTime(time))
    
    def gettime(self):
        return self._time.time().toPyTime() 
        
        