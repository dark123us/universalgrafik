#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, QtCore

class FreezeTable(QtGui.QTableView):
    freezeColumnCount = 3   #количество фиксированных колонок 
    rowCount = 3            #количество строк    
    headername = [u'№', u'Фамилия', u'Часов', '1', '2', '3', '4']#подписанные заголовки
    freeColumnCount = 4     #количество не фиксированных колонок

    def __init__(self, parent=None, mysql=None):
        self.initValues()
        QtGui.QTableView.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.initClasses()
        self._initWidget()
        self.initWidget()
        self.initAction()
    
    def initAction(self):
        self.connect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
        self.connect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
        self.connect(self.frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))

    def initValues(self):
        try: self.freezeColumnCount 
        except: self.freezeColumnCount = 0
        try: self.freeColumnCount 
        except: self.freeColumnCount = 1
        try: self.headername 
        except: self.headername = []
    
    def initClasses(self):
        self.mdl = QtGui.QStandardItemModel()
        self.setModel(self.mdl)
        self.mdls = QtGui.QItemSelectionModel(self.mdl)
        self.setSelectionModel(self.mdls)
        self.frozenTableView = QtGui.QTableView(self)
        #self.horizontalHeader().setStretchLastSection(True)
        
    def _initWidget(self):
        self.frozenTableView.setModel(self.model())
        self.frozenTableView.setFocusPolicy(QtCore.Qt.NoFocus)
        self.frozenTableView.verticalHeader().hide()
        self.frozenTableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
        self.viewport().stackUnder(self.frozenTableView)
        self.frozenTableView.setStyleSheet("QTableView { border: none;"
                                      "background-color: #8EDE21;"
                                      "selection-background-color: #999}")
        self.frozenTableView.setSelectionModel(self.selectionModel())
        self.frozenTableView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.frozenTableView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollMode(self.ScrollPerPixel)
        self.setVerticalScrollMode(self.ScrollPerPixel)
        self.frozenTableView.setVerticalScrollMode(self.ScrollPerPixel)
        
        self.frozenTableView.show()

    def initWidget(self):
        for col in range(self.freezeColumnCount,self.model().columnCount()):
            self.frozenTableView.setColumnHidden(col, True)
        for w in range(0,self.freezeColumnCount):
            self.frozenTableView.setColumnWidth(w, self.columnWidth(w))
        for w in range(0,self.model().rowCount()):
            self.frozenTableView.setRowHeight(w, self.rowHeight(w))
        self.updateFrozenTableGeometry()

    def updateSectionWidth(self,logicalIndex,i,newSize):
        if logicalIndex <= self.freezeColumnCount:
            self.frozenTableView.setColumnWidth(logicalIndex,newSize)
            self.updateFrozenTableGeometry()

    def updateSectionHeight(self, logicalIndex, i, newSize):
        self.frozenTableView.setRowHeight(logicalIndex, newSize)

    def resizeEvent(self,event):
        super(QtGui.QTableView, self).resizeEvent(event)
        self.updateFrozenTableGeometry()

    def moveCursor(self, cursorAction, modifiers):
        current = QtGui.QTableView.moveCursor(self, cursorAction, modifiers)
        if (cursorAction == self.MoveLeft and current.column()>0
                and  self.visualRect(current).topLeft().x() < self.frozenTableView.columnWidth(0) ):
            newValue = self.horizontalScrollBar().value() + self.visualRect(current).topLeft().x() - self.frozenTableView.columnWidth(0)
            self.horizontalScrollBar().setValue(newValue)
        return current

    def scrollTo(self, index, hint):
        if (index.column()>0):
            QtGui.QTableView.scrollTo(self, index, hint)

    def updateFrozenTableGeometry(self):
        size = 0
        for i in range(0,self.freezeColumnCount):
            size += self.columnWidth(i)
        self.frozenTableView.setGeometry(self.verticalHeader().width() + self.frameWidth(),
            self.frameWidth(),
            size,
            self.viewport().height()+self.horizontalHeader().height())
    
    def headredraw(self):
        self.mdl.setColumnCount(self.freezeColumnCount)
        self.mdl.setHorizontalHeaderLabels(self.headername)
        self.mdl.setRowCount(self.rowCount)
        self.mdl.setColumnCount(self.freeColumnCount+self.freezeColumnCount)
        self.initWidget()
        
    def footredraw(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()
    
    def redraw(self):
        self.headredraw()
        self._redraw()
        self.footredraw()
    
    def _redraw(self):
        raise Exception('need reinitilaze _redraw')

    def itemtable(self, row, col, mess, type = 0):
        "Способ отрисовки ячейки"
        #0 - не изменять атрибуты
        #1 - пометка ошибки
        #2 - праздничный
        #4 - суббота воскресенье
        
        item = QtGui.QStandardItem("%s"%mess)
        if col >= self.freezeColumnCount:
            if type & 1:
                cl = QtGui.QColor()
                cl.setNamedColor('#ff0000')
                br = QtGui.QBrush(cl)
                item.setBackground(br)                
            elif type & 2:#512( праздничный)
                if type & 4:#32+64 (суббота, воскресенье) 
                    cl = QtGui.QColor()
                    cl.setNamedColor('#ff7f7f')
                    br = QtGui.QBrush(cl)
                    item.setBackground(br)
                else:
                    cl = QtGui.QColor()
                    cl.setNamedColor('#ff8f8f')
                    br = QtGui.QBrush(cl)
                    item.setBackground(br)            
            elif type & 4:#32+64 - суббота или воскресенье
                br = QtGui.QBrush(QtCore.Qt.lightGray)
                item.setBackground(br)
        self.mdl.setItem(row,col,item)

