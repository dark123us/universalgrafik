#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
import sys
from data import FioModel
from sqlalchemy import create_engine
from util import DataBase
from table import TableControl
from datetime import date

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Планирование работы')
        self.central()
        self._engine = create_engine('mysql+mysqldb://root:234912234912@localhost/grafik?charset=utf8', echo=True)
        #для создания базы снять комментарий
        #DataBase(self._engine)
        self.grafik = TableControl(self._engine)
        lbl1 = QtGui.QLabel(u'Дата графика')
        calndr = QtGui.QDateEdit()
        calndr.setCalendarPopup(True)
        calndr.setDisplayFormat('MMMM yyyy')
        self.connect(calndr, SIGNAL('dateChanged (QDate)'), self.datechanged)
        calndr.setDate(QtCore.QDate(date.today()))
        hl = QtGui.QHBoxLayout()
        hl.addWidget(lbl1)
        hl.addWidget(calndr)
        hl.addStretch(1)
        gl = QtGui.QGridLayout()
        gl.addLayout(hl, 0, 0)
        gl.addWidget(self.grafik.getview())
        w = QtGui.QWidget()
        w.setLayout(gl)
        self.setCentralWidget(w)
        self.setMinimumSize(600, 400)
        
    def central(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)
    
    def datechanged(self, curdate):
        self.grafik.setdate(curdate.toPyDate())
        print (curdate.toPyDate(),)