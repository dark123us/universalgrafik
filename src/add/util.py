#!/usr/bin/python
# -*- coding: UTF-8  -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Time, Date, Interval, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, backref
from datetime import date, timedelta

Base = declarative_base()

class Model(object):
    _lastid = 0
    def __init__(self, engine):
        Session = sessionmaker(bind=engine)
        self._session = Session()
        self.observers = []
    def addobserver(self, inobserver):
        self.observers.append(inobserver)
    def notify(self):
        for i in self.observers:
            i.modelischanged()
    def removeobserver(self, inobserver):
        self.observers.remove(inobserver)
#//////////////////////////////////////////////////////////////////////////////////////
class DataBase(object):
    def __init__(self, engine):
        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)
        
        Session = sessionmaker(bind=engine)
        self.session = Session()
        self.default()
    
    def default(self):
        #id = 1 для обработки больничного
        #id = 2 для обработки донорских
        t = []
        t += [Target(u'Б', u'Больничный', 3, 2 )] 
        t += [Target(u'Д', u'Донорские', 4, 0, timedelta(0,28800), timedelta(0,61200))]
        t += [Target(u'1', u'1 Смена', 0, 1, timedelta(0,28800), timedelta(0,72000), 1)]
        t += [Target(u'2', u'2 Смена', 1, 1, timedelta(0,72000), timedelta(1,28800), 1, 12)]
        t += [Target(u'1п', u'Помощь', 2, 0, timedelta(0,28800), timedelta(0,61200))]
        t += [Target(u'О', u'Отсыпной', 5)]
        t += [Target(u'В', u'Выходной', 6)]
        t += [Target(u'Отп', u'Отпуск', 7)]
        t += [Target(u'Л', u'Льготный', 8)]
        t += [Target(u'П', u'Прогул', 9)]
        t += [Target(u'Бс', u'Без сохранения', 10)]
        t += [Target(u'1 о', u'1 Смена (за отгул)', 11)]
        t += [Target(u'2 о', u'2 Смена (за отгул)', 12)]
        t += [Target(u'Отг', u'Отдача отгула', 13)]
        for i in t:
            self.session.add(i)
        
        d1 = Dolg(u'диспетчер'); self.session.add(d1)
        print d1
        d2 = Dolg(u'уборщик'); self.session.add(d2)
        print d2
        self.session.commit()
        
        self.session.add(FIO(u'Тест Т.Т.', 1001, d1.id, date(2012,11,10)))
        self.session.add(FIO(u'Тест2 Т.Т.', 1002, d1.id, date(2012,10,10)))
        self.session.add(FIO(u'Тест3 Т.Т.', 1003, d2.id, date(2012,10,10)))
        
        for i in range(14):
            self.session.add(DolgTarget(d1.id, t[i].id))
            self.session.add(DolgTarget(d2.id, t[i].id))
        
        self.session.add(Setka(d1.id, t[2].id, 0))
        self.session.add(Setka(d1.id, t[3].id, 1))
        self.session.add(Setka(d1.id, t[4].id, 2))
        self.session.add(Setka(d1.id, t[5].id, 3))
        self.session.commit()
#//////////////////////////////////////////////////////////////////////////////////////
class Dolg(Base):
    'Должность'    
    __tablename__ = 'dolg'
    id   = Column(Integer, primary_key=True)
    name = Column(String(100))  #название должности
    def __init__(self, name):
        self.name = name
#//////////////////////////////////////////////////////////////////////////////////////
class FIO(Base):
    'ФИО'
    __tablename__ = 'fio'
    id        = Column(Integer, primary_key=True)
    fio       = Column(String(200)) #Ф.И.О.
    tabel     = Column(Integer)     #табельный номер
    datebegin = Column(Date)        #дата начала работы
    dateend   = Column(Date)        #дата завершения работы
    dolgid    = Column(Integer, ForeignKey(Dolg.id))     #ссылка на должность
    dolg      = relationship(Dolg)
    def __init__(self, fio, tabel, dolgid, datebegin = date.today(), dateend = None):
        self.fio        = fio
        self.tabel      = tabel
        self.dolgid     = dolgid
        self.datebegin  = datebegin
        self.dateend    = dateend 
#//////////////////////////////////////////////////////////////////////////////////////
class Target(Base):
    'Тип работы в графике'
    __tablename__ = 'target'
    id          = Column(Integer, primary_key=True)
    name        = Column(String(50)) #название в графике
    namepopup   = Column(String(50)) #название для меню
    daybegin    = Column(Interval)   #начало работы в секундах timestamp
    dayend      = Column(Interval)   #конец работы в секундах
    obed        = Column(Integer)    #в часах на обед
    night       = Column(Integer)    #в часах ночных
    sort        = Column(Integer)    #сортировка для меню, в каком порядке вывести
    typetarget  = Column(Integer)    #тип цели и как реагировать при выборе в меню,
        #0 - просто повторить во всех ячейках, 1 - использовать сетку при заполнении
        #2 - больничный
    
    def __init__(self, name, namepopup,
                 sort = 0,
                 typetarget = 0, 
                 daybegin = timedelta(0,0), 
                 dayend = timedelta(0,0), 
                 obed = 0,
                 night = 0):
        self.name = name
        self.namepopup = namepopup
        self.daybegin = daybegin 
        self.dayend = dayend
        self.obed = obed
        self.night = night
        self.sort = sort 
        self.typetarget = typetarget 
#//////////////////////////////////////////////////////////////////////////////////////
class Grafik(Base):
    'значение в графике'
    __tablename__ = 'grafik'
    id       = Column(Integer, primary_key=True)
    fioid    = Column(Integer, ForeignKey(FIO.id))  #ссылка на фио
    fio      = relationship(FIO)
    targetid = Column(Integer, ForeignKey(Target.id))  #ссылка на цель
    day      = Column(Date)     #ссылка на день
    minute   = Column(Integer)  #корректировка времени для подгонки под баланс
#//////////////////////////////////////////////////////////////////////////////////////
class Prazdnik(Base):
    'Праздничные'    
    __tablename__ = 'prazdnik'
    id   = Column(Integer, primary_key=True)
    name = Column(String(100))  #название праздника
    day  = Column(Date)         #день праздника
#//////////////////////////////////////////////////////////////////////////////////////
class DolgTarget(Base):
    'Перекрестная таблица между должностью и какие смены могут быть на этой должности'
    'Для попапа'
    __tablename__ = 'dolgtarget'
    id       = Column(Integer, primary_key=True)
    dolgid   = Column(Integer, ForeignKey(Dolg.id)) #ссылка на id в должности
    dolg     = relationship(Dolg)
    targetid = Column(Integer, ForeignKey(Target.id)) #ссылка на id в цели
    target   = relationship(Target)
    def __init__(self, dolgid, targetid):
        self.dolgid     = dolgid
        self.targetid   = targetid
#//////////////////////////////////////////////////////////////////////////////////////
class Setka(Base):
    'Порядок заполнения графика - аля сетка'
    __tablename__ = 'setka'
    id        = Column(Integer, primary_key=True)
    dolgid    = Column(Integer, ForeignKey(Dolg.id)) #ссылка на id в должности
    dolg      = relationship(Dolg)
    targetid  = Column(Integer, ForeignKey(Target.id)) #ссылка на id в цели
    target    = relationship(Target)    
    sort      = Column(Integer) #порядок в каком заполнять данные  
    def __init__(self, dolgid, targetid, sort):
        self.dolgid     = dolgid
        self.targetid   = targetid
        self.sort       = sort
